# Student Account Manager server demo for cis433
# Author: Liwei Fan

from flask import Flask, jsonify
from flask import render_template, request
from flask_sqlalchemy import SQLAlchemy
import json
import sys
sys.path.append("..")
from manager_setup.tables import User

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"]  = 'mysql+pymysql://localuser:localuser@127.0.0.1/students'
app.config["SQLALCHEMY_ECHO"] = False
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

@app.route('/')
def hello():
    return render_template('index.html')

@app.route('/deal_request', methods=['GET'])
#def deal_request():
#    # using query()
#    username = request.args.get("username","")
#    password = request.args.get("password","")
#    res = db.session.query(User).filter(User.username==username,User.password==password).first()
#    if not res:
#        return render_template("index.html",username=username,password=password, is_auth="false")
#    resjson = res._to_json()
#    if not password == resjson["password"]:
#        return render_template("index.html",username=username,password=password)
#    else:
#        email = resjson["email"]
#        info = resjson["info"]
#        return render_template("result.html",username=username,email=email, info=info)
    

def deal_request():
    # using execute
    username = request.args.get("username","")
    password = request.args.get("password","")
    resjson = db.session.execute("select * from user where username=\'%s\' and password=\'%s\'"%(username,password)).first()
    if not resjson:
        return render_template("index.html", is_auth="false")
    else:
        print(list(resjson))
        email = list(resjson)[3]
        info = list(resjson)[4]
        return render_template("result.html",username=username,email=email,info=info)
    return render_template("index.html", is_auth="false")
    

def show_page():
    pass
    

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True)
