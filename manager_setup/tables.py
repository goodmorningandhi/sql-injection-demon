from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://localuser:localuser@127.0.0.1/students'
app.config["SQLALCHEMY_ECHO"] = False
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(20))
    email = db.Column(db.String(120))
    info = db.Column(db.Text)

    def __init__(self, username, password, email, info=""):
        self.username = username
        self.password = password
        self.email = email
        self.info = info

    def _to_json(self):
        return {"id":self.id,
                "username":self.username,
                "password":self.password,
                "email":self.email,
                "info":self.info}

    def __repr__(self):
        return '<User %r>' % self.username

if __name__ == "__main__":
    db.create_all()