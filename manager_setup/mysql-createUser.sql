GRANT ALL PRIVILEGES ON *.* TO 'localuser'@'localhost' IDENTIFIED BY 'localuser' WITH GRANT OPTION;

CREATE DATABASE IF NOT EXISTS students DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

flush privileges;
