# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repo is for cis433 project demo
* Based on Python3.6 flask for server-client and mariaDB for database
* Web-based api
* [CIS433 crouse website](http://classes.cs.uoregon.edu/21W/cis433/)

### How do I get set up? ###

* Initial the mysql user:
  $ mysql < manager_setup/mysql-createUser.sql
* Create the table:
  $ python3 manager_setup/tables.py
* Register an account:
  $ mysql -ulocaluser -plocaluser
  mysql > use students;
  mysql[students] > INSERT INTO user (username, password, email, info) 
                    -> values 
                    -> ("user1", "pass1", "user1@email.com", "This is user1's info");
* Run server:
  $ cd app/
  $ python3 test_server.py
* Browser for client UI:
  localhost:8000

### Who do I talk to? ###

* Repo admins: 
* Liwei Fan `liweif@uoregon.edu`
* Lambert (Xingzhi Wang) `xingzhiw@uoregon.edu`
